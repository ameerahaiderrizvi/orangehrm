#!/bin/bash

# Update the system
sudo yum update -y

# Install Docker
sudo amazon-linux-extras install docker -y
sudo systemctl start docker
sudo usermod -a -G docker ec2-user
sudo systemctl enable docker

# Install required Python packages for Flask app
sudo yum install -y python3-pip

# Install Python 3.8 and pip for Dockerfile
sudo amazon-linux-extras enable python3.8
sudo yum install -y python3.8
sudo python3.8 -m ensurepip --upgrade
sudo pip3.8 install --upgrade pip

# Install Apache HTTP Server (httpd)
sudo yum install -y httpd
sudo systemctl start httpd
sudo systemctl enable httpd

# Create the directory for the test.html file
sudo mkdir -p /var/www/html

# Create the test.html file for health check
echo "Health check passed" | sudo tee /var/www/html/test.html



#!/bin/bash

# Update the system
sudo yum update -y

# Install Apache, Git, and Docker
sudo yum install -y httpd24 git
sudo amazon-linux-extras install docker -y

# Start and enable Apache
sudo service httpd start
sudo systemctl enable httpd

# Start and enable Docker
sudo service docker start
sudo usermod -a -G docker ec2-user

# Clone OrangeHRM repository
sudo git clone https://github.com/orangehrm/orangehrm.git /var/www/html/orangehrm
sudo chown -R ec2-user:apache /var/www/html/orangehrm
sudo chmod -R 755 /var/www/html/orangehrm

# Navigate to OrangeHRM directory
cd /var/www/html/orangehrm

# Build and run OrangeHRM Docker container
sudo docker build -t orangehrm .
sudo docker run -d -p 8080:80 --name orangehrm-container orangehrm

# Restart Apache
sudo service httpd restart
